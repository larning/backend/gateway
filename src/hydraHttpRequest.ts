import { IHydra } from "hydra";

export enum RequestMethod {
    GET = "GET",
    POST = "POST",
    PUT = "PUT",
    DELETE = "DELETE",
}

export default class HydraRequest {
    serviceName: string;
    hydra: IHydra;
    constructor(hydra: IHydra) {
        this.hydra = hydra;
        const serviceName = hydra.getServiceName();
        if (serviceName == undefined || typeof serviceName != "string") throw new Error("Failed to parse config: hydra.serviceName is a required string.");
        this.serviceName = serviceName;
    }

    async send(
        service: string,
        method: RequestMethod,
        path: string,
        body?: string | { [key in string | number]?: unknown },
        headers?: {[key: string]: string}
    ): Promise<{ statusCode?: number; statusMessage?: string; statusDescription?: string; body?: string | { [key in string | number]: unknown }; headers?: { [key: string]: string } }> {
        const response: { [key: string]: unknown } = await this.hydra.makeAPIRequest({
            to: `${service}:[${method.toString()}]${path.startsWith("/") ? path : `/${path}`}`,
            from: `${this.serviceName}:/`,
            body: body ?? "",
            headers
        });
        if ((response.payLoad ?? response) == undefined) {
            throw new Error(`Error parsing response from service.`);
        }
        const result = {
            statusCode: response.statusCode as number | undefined,
            statusMessage: response.statusMessage as string | undefined,
            statusDescription: response.statusDescription as string | undefined,
            headers: response.headers as { [key: string]: string } | undefined,
            body: {},
        };
        result.body =
            response.payLoad != null
                ? (response.payLoad as Buffer).toString()
                : (() => {
                      const internalKeys = Object.keys(result);
                      const resultObj: { [key: string]: unknown } =
                          Object.keys(response).includes("result") && Object.entries(response.result as object).length != 0 ? (response.result as { [key in string | number]?: unknown }) : {};
                      if (Object.entries(resultObj).length == 0)
                          Object.entries(response).forEach(([key, val]) => {
                              if (!internalKeys.includes(key) && !(key == "result" && Object.entries(val as object).length == 0)) resultObj[key] = val;
                          });
                      return resultObj;
                  })();
        return result;
    }
}
