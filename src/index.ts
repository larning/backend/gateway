import hydra from "hydra";
import fs from "fs";
import HydraRequest, { RequestMethod } from "./hydraHttpRequest";
import { ApolloGateway, GraphQLDataSource } from "@apollo/gateway";
import HydraApolloDataSource from "./hydraApolloDataSource";
import { ApolloServer } from "apollo-server";

async function main() {
    const config = JSON.parse((await fs.promises.readFile("./config.json")).toString());
    await hydra.init(config, false);

    const hydraHttpRequest = new HydraRequest(hydra);

    const services: { [key: string]: unknown }[] = await hydra.getServices();

    const gateway = new ApolloGateway({
        serviceList: services.map(service => ({ name: service.serviceName as string, url: service.serviceName as string })),
        buildService: ({ name }): GraphQLDataSource => new HydraApolloDataSource({ name: name as string, path: "/gql" }, hydraHttpRequest),
    });

    const server = new ApolloServer({
        gateway,
        subscriptions: false,
    });
    server.listen()  
}

main();
